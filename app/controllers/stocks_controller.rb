class StocksController < ApplicationController
  include ActionController::MimeResponds
  respond_to :json

  def index
    respond_with Stock.all
  end
end
