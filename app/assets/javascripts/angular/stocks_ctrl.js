/**
 * Created by hammad on 06/06/15.
 */

app.controller('StocksCtrl', ['$scope', '$resource', function($scope, $resource) {
    var Stocks = $resource('/api/stocks');
    $scope.stocks = Stocks.query(); /* this is a ngresourse method 'query': {method: 'GET', isArray: true}*/
}]);